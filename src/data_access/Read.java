package data_access;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;

import model.MonitoredData;

public class Read {
	
    private List<MonitoredData> activities; //list of activities
    private List<LocalDate> date; //list of dates for each activity

   
    public Read() throws IOException {
        this.readFile();
    }
 
	public List<MonitoredData> getActivities() { //get List of Activities
		return activities;
	}


	public void setActivities(List<MonitoredData> activities) { //set List of Activities
		this.activities = activities;
	}


	public List<LocalDate> getDate() { //get List of Dates
		return date;
	}


	public void setDate(List<LocalDate> date) { //set List of Dates
		this.date = date;
	}
	
	 private void readFile() throws IOException 
	    {
	        try (Stream<String> stream = Files.lines(Paths.get("Activity.txt"))) 
	        {
	            activities=stream.map(s -> s.split("\\s\\s+")).map(input->new MonitoredData(input[0], input[1], input[2])).collect(Collectors.toList());
	        }
	        catch(IOException e) 
	        {
	            e.printStackTrace();
	        }
	    }

}
