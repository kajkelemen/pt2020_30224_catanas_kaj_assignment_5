package main;

import java.io.IOException;

import business_logic.*;

public class MainClass {

    public static void main(String[] args) throws IOException {

        System.out.println("Hello World!");
        
        new Task1();
        new Task2();
        new Task3();
        new Task4();
        new Task5();

    }
}