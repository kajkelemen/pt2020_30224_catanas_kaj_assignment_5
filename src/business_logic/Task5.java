package business_logic;

import java.io.IOException;
import java.io.File;
import java.io.FileOutputStream;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.stream.Collectors;

import data_access.Read;

import java.util.List;

public class Task5 {

	private DateTimeFormatter time = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

	public Task5() throws IOException
	{
		Read r1=new Read();
		timeSpent(r1);
	}

	//For each activity compute the entire duration over the monitoring period
	private void timeSpent(Read read) throws IOException
	{
		File file=new File("TASK_5.txt");
		FileOutputStream out=new FileOutputStream(file);
	
		Map<String,Duration> map=new HashMap<String, Duration>();
		
		Duration[] d= {Duration.ZERO};
		LocalTime midnight=LocalTime.of(23, 59,59);
		
		List<LocalTime> t1=new ArrayList<LocalTime>();
		List<LocalTime> t2=new ArrayList<LocalTime>();
		List<String> activity=new ArrayList<String>();
		
		read.getActivities().stream().map(e->e.getActivityLabel()).
			distinct().map(e->activity.add(e)).collect(Collectors.toList());
		
		read.getActivities().stream().map(e->(LocalDateTime.parse(e.getStartTime(),time)).toLocalTime()).
			distinct().map(e->t1.add(e)).collect(Collectors.toList());
		
		read.getActivities().stream().map(e->(LocalDateTime.parse(e.getEndTime(),time)).toLocalTime()).
			distinct().map(e->t2.add(e)).collect(Collectors.toList());
		
		
		activity.stream().forEach(e->{
			
			d[0]=Duration.ZERO;
			
			read.getActivities().stream().forEach(j->
			{
				if(j.getActivityLabel().equals(e))
				{
					LocalTime startTime,endTime;
					
					startTime=t1.get(read.getActivities().indexOf(j));
					endTime=t2.get(read.getActivities().indexOf(j));
					Duration diff=Duration.between(startTime, endTime);
					
					if(diff.getSeconds()>0) d[0]=d[0].plus(diff);
					
					if(diff.getSeconds()<0)
					{
						Duration part1=Duration.between(startTime, midnight);
						Duration part2=Duration.between(LocalTime.of(0,0,0), endTime);
						d[0]=(d[0].plus(part1.plus(part2).plusSeconds(1)));
					}
				}
			});
			map.put(e, d[0]);			
		});
		map.entrySet().stream().forEach(e->{
			try 
			{
				out.write(("Activity: "+e.getKey()+" lasts:"+e.getValue().toHours()+" hours"+(e.getValue().toMinutes())%60+" minutes"+(e.getValue().getSeconds())%60+" seconds\n").getBytes());
			} 
			catch (IOException e1) 
			{
				e1.printStackTrace();
			}
		});
		out.close();
	}
	
}
