package business_logic;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import data_access.Read;

public class Task1 {

	
	 public Task1() throws IOException
	 {
		 Read r1=new Read();
		 displayData(r1);
	 }
	 
	 
     //Display data for each Activity
	 private void displayData(Read read) throws IOException {
	        
		  File file = new File("TASK_1.txt");
	      FileOutputStream out=new FileOutputStream(file);
	      
	      read.getActivities().stream().forEach(s->{
	          try 
	          {
	              out.write(("start: " + s.getStartTime() + "|  end: "+s.getEndTime()+"|  activity:"+s.getActivityLabel()+ "\n" ).getBytes());
	          } 
	          catch (IOException e) 
	          {
	              e.printStackTrace();
	          }
	      });
	      out.close();
	 }
}
