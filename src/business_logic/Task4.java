package business_logic;

import java.io.IOException;
import java.io.File;
import java.io.FileOutputStream;

//import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import data_access.Read;
import model.MonitoredData;

//import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Task4 {
	
	 private DateTimeFormatter time = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

	public Task4() throws IOException
	{
		Read r1=new Read();
		countEachActivityForEachDay(r1);
	}	
	
	//Count for how many times each activity has appeared for each day over the monitoring period.
	private void countEachActivityForEachDay(Read read) throws IOException{
		 
			File file=new File("TASK_4.txt");
			FileOutputStream out=new FileOutputStream(file);
			
			Map<Integer,Map<String,Integer>> map=read.getActivities().stream().collect(Collectors.groupingBy(
					e->LocalDateTime.parse(e.getStartTime(),time).getDayOfMonth(),Collectors.groupingBy(
							MonitoredData::getActivityLabel,Collectors.summingInt(e->1))));
			
			map.entrySet().stream().forEach(e->{
				try 
				{
					out.write((e.getKey()+":"+e.getValue().entrySet()+"\n").getBytes());
				} 
				catch (IOException e1) 
				{
					e1.printStackTrace();
				}
			});
			out.close();
		}
}

