package business_logic;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Map;
import java.util.stream.Collectors;

import data_access.Read;
import model.MonitoredData;

public class Task3 {

	
	public Task3() throws IOException
	{
		Read r3=new Read();
		countEachActivity(r3);
	}

	
	//Count how many times each activity has appeared over the entire monitoring period.
    private void countEachActivity(Read read)throws IOException 
    {
        File file=new File("TASK_3.txt");
        FileOutputStream out=new FileOutputStream(file);
        
        Map<String, Integer> activity=read.getActivities().stream().collect(Collectors.groupingBy(
        		MonitoredData::getActivityLabel,Collectors.reducing(0,e->1,Integer::sum)));
        
        activity.forEach((key,value)->{
            try 
            {
            	out.write((key+": "+ value + "\n").getBytes());
            } 
            catch (IOException e1) 
            {
                e1.printStackTrace();
            }
        });
        out.close();
    }
}
