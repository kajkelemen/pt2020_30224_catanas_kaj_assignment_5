package business_logic;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

import data_access.Read;

public class Task2 {

	 private DateTimeFormatter time = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
	 private int days=1; //count distinct days
	
	public Task2() throws IOException
	{
		Read r2=new Read();
		countDistinctDays(r2);
	}
	
	
	//Count the distinct days that appear in the monitoring data.
	private void countDistinctDays(Read read)throws IOException 
    {
        File file=new File("TASK_2.txt");
        FileOutputStream out=new FileOutputStream(file);

        List<LocalDateTime> dateTime=read.getActivities().stream().map(l-> LocalDateTime.parse(l.getEndTime(),time)).collect(Collectors.toList());
        List<LocalDate> date=dateTime.stream().map(l->l.toLocalDate()).collect(Collectors.toList());
        
        read.setDate(date.stream().distinct().collect(Collectors.toList()));
        read.getDate().stream().forEach(s->{ 
            try 
            {
				out.write(("Day: "+ days +" : "+s.getDayOfWeek()+" "+ s + "\n").getBytes());
                days++;
              
            }
            catch (IOException e) 
            {
                e.printStackTrace();
            }
        });
        out.close();
    }
}
